%% Axes: Insert the matrix equation for the model.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function image = addImage(w, h, f)

    %%
    % Create the axes.
    % 
    image = axes('Units', 'pixels', 'Tag', 'img', 'Parent', f, ...
        'Position', [1/20*w 2/12*h 2/5*w 1/3*h], 'Units', 'normalized');
    
    %%
    % Insert picture.
    % 
    img = imread('intro.png','BackgroundColor',[1 1 1]);
%     axes(guihandles(f).axes1);
%     img = imresize(img, 0.3);
    imshow(img);

    
    %%
    % Set dimensions and turn axes off.
    % 
    axis([-10 10 -10 10]);
    axis off;
end