%% Axes: Draw the block diagram.
% Returns the axes on which the diagram is drawn. w and h represent the
% width and height of the figure window, respectively. f is the parent
% figure.
function blockDiag = drawBlockDiag(w, h, f)

    %%
    % Create the axes.
    % 
    blockDiag = axes('Units', 'pixels', 'Tag', 'blockDiag', ...
        'Parent', f, 'Position', [1/20*w 7/12*h 2/5*w 1/3*h], ...
        'Units', 'normalized');
    
    %%
    % Create the central and effect-site compartments. Both are initially
    % filled with white, representing 0 concentration.
    % 
    % * $$ x_{C}(t) $$ is the drug centration in the central compartment
    % (cent).
    % * $$ x_{E}(t) $$ is the drug centration in the effect-site
    % compartment (eff).
    % 
    centX = [1 3 3 1 1];
    centY = [-1 -1 1 1 -1];
    effX = centX + 3;
    effY = centY;
    fill(centX, centY, [1 1 1]);    % red
    hold on
    fill(effX, effY, [1 1 1]);      % blue
    hold on
    
    %%
    % Create the arrows representing flow in, out, and between the two
    % compartments.
    % 
    % * $$ u(t) $$ is the rate of infusion of the anesthetic drug
    % * $$ k_{ce} $$ is the rate of flow from the central compartment to
    % the effect-site compartment.
    % * $$ k_{ec} $$ is the rate of flow from the effect-site compartment
    % to the central compartment.
    % * $$ k_{co} $$ is the rate of elimination from the central
    % compartment.
    % 
    plotArrow('r', 0, 0, 1);       % u(t)
    plotArrow('r', 3, 0.25, 1);    % k_ce
    plotArrow('l', 4, -0.25, 1);   % k_ec
    plotArrow('d', 2, -1, 1.25);   % k_co
    
    %%
    % Insert text labels for the components of the block diagram.
    % 
    text(0.25, 0.25, 'u(t)', 'FontSize', 15);
    text(3.3, 0.5, 'k_{ce}', 'FontSize', 15);
    text(3.4, -0.5, 'k_{ec}', 'FontSize', 15');
    text(1.5, -1.5, 'k_{co}', 'FontSize', 15);
    text(1.75, 1.25, 'x_{C}(t)', 'FontSize', 15);
    text(4.75, 1.25, 'x_{E}(t)', 'FontSize', 15);
    
    %% 
    % Set dimensions and turn axes off.
    % 
    axis([0 6 -2.5 1.5]);
    axis off;
end