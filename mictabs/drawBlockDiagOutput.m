%% Axes: Draw the block diagram for feedback control/estimator.
% Returns the axes on which the diagram is drawn. w and h represent the
% width and height of the figure window, respectively. f is the parent
% figure.
% type is a variable indicating if the diagram is for estimator, state, or
% output feedback control.
function blockDiagFeed = drawBlockDiagOutput(w, h, f, type)

    %%
    % Create the axes.
    % 
    blockDiagFeed = axes('Units', 'pixels', 'Tag', 'blockDiag', ...
        'Parent', f, 'Position', [1/20*w 7/12*h 2/5*w 1/3*h], ...
        'Units', 'normalized');
    
    %%
    % Create the compartments.
    % 
    % * H is the compartmental model.
    % * E is the estimator.
    % * K is the feedback controller.
    % 
    H_X = [2 4 4 2 2];
    H_Y = [-1 -1 1 1 -1];
    fill(H_X, H_Y, [1 1 1]);
    hold on;
    text(3, 0, 'H', 'FontSize', 15);
    % other information...
    E_X = H_X + 4.5;
    E_Y = H_Y - 2;
    K_X = H_X;
    K_Y = H_Y - 4;
    
    if strcmp(type, 'estimator') == 1       % estimator
        fill(E_X, E_Y, [1 1 1]);
        hold on;
        text(7.5, -2, 'E', 'FontSize', 15);
        
        plot([1,1], [0,-2], 'k', 'LineWidth', 2);   % towards E
        hold on;
        plotArrow('r', 1, -2, 5.5);
        plotArrow('d', 7.5, 0, 1);
        
        plot([7.5,7.5], [-3,-4], 'k', 'LineWidth', 2);
        hold on;
        plot([0,7.5], [-4,-4], 'k', 'LineWidth', 2);
        hold on;
    elseif strcmp(type, 'state') == 1           % state feedback control
        fill(K_X, K_Y, [1 1 1]);
        text(3, -4, 'K', 'FontSize', 15);
        
        plot([7.5,7.5], [0,-4], 'k', 'LineWidth', 2);
        hold on;
        
        plotArrow('l', 7.5, -4, 3.5);   % xhat(t)
        text(5, -3.5, 'xhat(t)', 'FontSize', 12);
        
        plot([0,2], [-4,-4], 'k', 'LineWidth', 2);
        hold on;
    elseif strcmp(type, 'output') == 1          % output feedback control
        fill(E_X, E_Y, [1 1 1]);
        text(7.5, -2, 'E', 'FontSize', 15);
        hold on;
        
        fill(K_X, K_Y, [1 1 1]);
        text(3, -4, 'K', 'FontSize', 15);
        
        plotArrow('l', 7.5, -4, 3.5);   % xhat(t)
        text(5, -3.5, 'xhat(t)', 'FontSize', 12);
        plot([0,2], [-4,-4], 'k', 'LineWidth', 2);
        
        plot([1,1], [0,-2], 'k', 'LineWidth', 2);   % towards E
        plotArrow('r', 1, -2, 5.5);
        plotArrow('d', 7.5, 0, 1);
        
        plot([7.5,7.5], [-3,-4], 'k', 'LineWidth', 2);
        hold on;
    end
    
    %%
    % Create the arrows representing flow between the two
    % compartments.
    % 
    % * $$ u(t) $$ is the drug flow into the model
    % * $$ y(t) $$ is the observable output
    % * $$ r(t) $$ is the input into the system
    % * $$ x(0) $$ is the initial state
    % * $$ \hat{x}(t) $$ is the estimated value of x
    % 
    hold on;
    plotArrow('r', -3, 0, 2.5);     % r(t)
    plotArrow('r', 0.5, 0, 1.5);    % u(t)
    plotArrow('r', 4, 0, 6);        % y(t)
    plotArrow('u', 0, -4, 3.5);     % up from K
    plot([3,3], [1,3], 'k', 'LineWidth', 2);  % x(0)
    hold on;
    pos = [-0.5 -0.5 1 1];
    rectangle('Position', pos, 'Curvature', [1 1], 'FaceColor', [1 1 1]);
    
    %%
    % Insert text labels for the components of the block diagram.
    % 
    text(-2, 0.5, 'r(t)', 'FontSize', 12);
    text(1, 0.5, 'u(t)', 'FontSize', 12);
    text(2, 2, 'x(0)', 'FontSize', 12);
    text(7.5, 0.5, 'y(t)', 'FontSize', 12);
    text(-0.1, 0, '+', 'FontSize', 15);
    
    %% 
    % Set dimensions and turn axes off.
    % 
    axis([-3 10 -5 3]);
    axis off;
end