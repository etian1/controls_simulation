%% Axes: Draw the complex axes to plot eigenvalues/vectors and poles.
%  
function complexAxes = drawComplexAxes(w, h, f, t)
complexAxes = axes('Units', 'pixels', 'Tag', 'blockDiag', ...
        'Parent', f, 'Position', [1/20*w 15/24*h 2/5*w 1/3*h], ...
        'Units', 'normalized');
    
    title(t);               % set title of plot
    hold on;
    ylim([-10 10]);
    xlabel('real');
    ylabel('imaginary');
    vline(0,'r-');


end