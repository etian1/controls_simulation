%% Axes: Draw the concentration plot.
%
function concPlot = drawConcPlot(w, h, f)
    concPlot = axes('Units', 'pixels', 'Tag', 'concPlot', ...
        'Parent', f, ...
        'Position', [1/20*w 1/8*h 9/20*w 2/5*h], ...
        'Units', 'normalized');
    title('Drug concentration in each compartment');
    axis([0 50 0 50]);
    xlabel('Time (h)');
    ylabel('Concentration (mg/mL)');
end