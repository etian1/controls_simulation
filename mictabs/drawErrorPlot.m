%% Axes: Plot the error with feedback control.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function responseAxes = drawErrorPlot(w, h, f)

    %%
    % Create the axes.
    % 
    responseAxes = axes('Units', 'pixels', 'Tag', 'rEq', 'Parent', f, ...
        'Position', [10/20*w 1/8*h 7/16*w 1/3*h], 'Units', 'normalized');
    
    %%
    % Set dimensions and parameters.
    % 
    title('Error');
%     axis([0 50 0 50]);
    xlabel('Time');
    ylabel('e(t)');
    hold on;
end