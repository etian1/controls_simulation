%% Axes: Insert the matrix equation for the model.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function stabilityEq = drawMatrixEqnStability(w, h, A, B, C, D, f)

    %%
    % Create the axes.
    % 
    stabilityEq = axes('Units', 'pixels', 'Tag', 'stabilityEq', 'Parent', f, ...
        'Position', [25/40*w 7/12*h 2/5*w 1/3*h], 'Units', 'normalized');
    
    %%
    % Insert the model equation.
    % 
    text(-0.5, 2, ['The transfer function can be represented by:'], 'FontSize', 13);
    
    % variable equation
    eq1 = ['$$ H(s) = C(sI-A)^{-1}B + D = $$'];
    
    text(0, 0, eq1, 'Interpreter', 'latex', 'FontSize', 16);
    
    % Display transfer function
    [numerator, denominator] = ss2tf(A, B, C, D);
    sys = tf(numerator, denominator);
    t = evalc('sys');
    t(1:6) = [];
    text(0, -3, t, 'FontSize', 13, 'Tag', 'transferFunction');


%     % A matrix
%     text(1.6, -2.5, mtx, 'Interpreter', 'latex', 'FontSize', 16, ...
%         'Tag', 'numMtx');
%     
%     % rest of equation
%     eq3 = ['$$ \left( \begin{array}{c} x_{C} \\ x_{E} $$' ...
%         '$$ \\ \end{array} \right) + $$' ...
%         '$$ \left( \begin{array}{c} 1 \\ 0 \\ \end{array} \right) u $$'];
%     text(6.87, -2.5, eq3, 'Interpreter', 'latex', 'FontSize', 16);
%     
%     eq4 = '$$ y = log(x_{E} + x_{C}) $$';
%     text(1.25, -5, eq4, 'Interpreter', 'latex', 'FontSize', 16);
    
    %%
    % Set dimensions and turn axes off.
    % 
    axis([0 10 -6 3]);
    axis off;
end