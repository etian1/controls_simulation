%% Axes: Insert the matrix equation for the model.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function mtxEq = drawMtxEq(w, h, Amtx, Cmtx,f)

    %%
    % Create the axes.
    % 
    mtxEq = axes('Units', 'pixels', 'Tag', 'mtxEq', 'Parent', f, ...
        'Position', [21/40*w 7/12*h 2/5*w 1/3*h], 'Units', 'normalized');
    
    %%
    % Insert the model equation.
    % 
    text(-0.5, 2, ['The 2-compartmental model can be represented by ' ...
        'the following equations:'], 'FontSize', 13);
    
    % variable equation
    eq1 = ['$$ \left( \begin{array}{c} \dot{x_{C}} \\ \dot{x_{E}} $$' ...
        '$$ \\ \end{array} \right) = $$' ...
        '$$ \left( \begin{array}{cc} 1-(k_{ce}+k_{co}) & k_{ec} $$' ...
        '$$ \\ k_{ce} & 1-k_{ec} \\ \end{array} \right)$$' ...
        '$$ \left( \begin{array}{c} x_{C} \\ x_{E} $$' ...
        '$$ \\ \end{array} \right) + $$' ...
        '$$ \left( \begin{array}{c} 1 \\ 0 \\ \end{array} \right) u $$'];
    text(0, 0, eq1, 'Interpreter', 'latex', 'FontSize', 16);
    
    % A matrix
    text(1.6, -2.5, Amtx, 'Interpreter', 'latex', 'FontSize', 16, ...
        'Tag', 'numMtx');
    
    % rest of equation
    eq3 = ['$$ \left( \begin{array}{c} x_{C} \\ x_{E} $$' ...
        '$$ \\ \end{array} \right) + $$' ...
        '$$ \left( \begin{array}{c} 1 \\ 0 \\ \end{array} \right) u $$'];
    text(6.87, -2.5, eq3, 'Interpreter', 'latex', 'FontSize', 16);
    
    % y = c1xe + c2xc
    eq4 ='$$ y = log(c_1 x_{E} + c_2 x_{C}) $$';
    text(1.25, -5, eq4, 'Interpreter', 'latex', 'FontSize', 16,...
        'Tag', 'yEq');
    
    text(6.87, -5, Cmtx, 'Interpreter', 'latex', 'FontSize', 16,...
        'Tag', 'Cmtx');
    
    % initial conditions
    eq5 ='$$ x_{0} = x(0) = 0 $$';
    text(1.25, -7, eq5, 'Interpreter', 'latex', 'FontSize', 16,...
        'Tag', 'yEq');
    
    %%
    % Set dimensions and turn axes off.
    % 
    axis([0 10 -6 3]);
    axis off;
end