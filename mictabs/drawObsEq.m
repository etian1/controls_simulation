%% Axes: Insert the details of the observability matrix.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function obsEq = drawObsEq(w, h, A, C, O, f)

    %%
    % Create the axes.
    % 
    obsEq = axes('Units', 'pixels', 'Tag', 'oEq', 'Parent', f, ...
        'Position', [25/40*w 7/12*h 2/5*w 1/3*h], 'Units', 'normalized');
    
    
    %%
    % Insert the reachability matrix.
    text(-0.5, 2, ['The observability matrix can be represented by:'], ...
        'FontSize', 13);
    
    eq1 = '$$ O = \left( \begin{array}{cc} C \\  CA \end{array} \right)$$';
    text(0, 0, eq1, 'Interpreter', 'latex', 'FontSize', 16);
    mtx = sprintf(['$$ = \\left( \\begin{array}{cc} %0.3g & %0.3g $$' ...
        '$$ \\\\ %0.3g & %0.3g \\\\ \\end{array} \\right)$$'], ...
        O(1,1), O(1,2), O(2,1), O(2,2));
    text(2.5, 0, mtx, 'Interpreter', 'latex', 'FontSize', 16, ...
        'Tag', 'oMtx');
    determinant = sprintf(['$$ det(O) = %0.3g $$'], det(O));
    text(0, -2.5, determinant, 'Interpreter', 'latex', 'FontSize', 16, ...
        'Tag', 'oDet');
    text(0, -5, 'A =', 'Interpreter', 'latex',  'FontSize', 16);
    Amtx = sprintf(['$$ \\left( \\begin{array}{cc} %0.3g & %0.3g $$' ...
        '$$ \\\\ %0.3g & %0.3g \\\\ \\end{array} \\right)$$'], ...
        A(1,1), A(1,2), A(2,1), A(2,2));
    text(1, -5, Amtx, 'Interpreter', 'latex',  'FontSize', 16, 'Tag', 'obsAMatrix');

    text(0, -8, 'C =', 'Interpreter', 'latex',  'FontSize', 16);
    Cmtx = sprintf(['$$ \\left( \\begin{array}{cc} %0.3g & %0.3g \\end{array} \\right)$$'], ...
        C(1), C(2));
    text(1, -8, Cmtx, 'Interpreter', 'latex',  'FontSize', 16, 'Tag', 'obsCMatrix');
    
    %%
    % Set dimensions and turn axes off.
    % 
    axis([0 10 -6 3]);
    axis off;
end