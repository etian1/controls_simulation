%% Axes: Insert the matrix equation for the model.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function obsThm = drawObsThm(w, h, f)

    %%
    % Create the axes.
    % 
    obsThm = axes('Units', 'pixels', 'Tag', 'obsThm', 'Parent', f, ...
        'Position', [1/20*w 2/12*h 2/5*w 1/3*h], 'Units', 'normalized');
    
    %%
    % Insert the model equation.
    % 
    message1 = sprintf(['Observability Test: \n']);
    
    text(-0.5, 12, message1, 'FontSize', 15);
    
    def1 = ['$$(A,C)$$ Observable $$\Longleftrightarrow det(O) \ne 0$$ '];
    
    text(-0.5, 10, def1, 'Interpreter', 'latex', 'FontSize', 15);

    %%
    % Set dimensions and turn axes off.
    % 
    axis([0 10 -6 3]);
    axis off;
end