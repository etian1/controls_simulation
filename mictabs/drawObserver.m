%% Axes: Draw the x_hat plot.
%
function x_hat_plot = drawObserver(w, h, f)
        x_hat_plot = axes('Units', 'pixels', 'Tag', 'rEq', 'Parent', f, ...
        'Position', [10/20*w 1/8*h 7/16*w 1/3*h], 'Units', 'normalized');
    title('Observer Performance');
%     axis([0 50 0 50]);
    xlabel('Time (h)');
    ylabel('Concentration (mg/mL)');
end