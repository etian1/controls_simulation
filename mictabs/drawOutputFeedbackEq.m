%% Axes: Insert the output feedback equation.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function outputFeedbackEq = drawOutputFeedbackEq(w, h, f)

    %%
    % Create the axes.
    % 
    outputFeedbackEq = axes('Units', 'pixels', 'Tag', 'mtxEq', 'Parent', f, ...
        'Position', [21/40*w 7/12*h 2/5*w 1/3*h], 'Units', 'normalized');
    
    %%
    % Insert the model equation.
    % 
    text(-0.5, 2, ['The output feedback control model (closed loop) ', ...
        'can be represented by:'], 'FontSize', 13);
    
    eq1 = ['$$ \dot{x}_{CL} = \left( \begin{array}{cc} A+BK & -BK $$', ...
        '$$ \\ 0 & A+LC \end{array} \right) $$', ...
        '$$ \left( \begin{array}{c} x \\ e \end{array} \right) $$', ...
        '$$ + \left( \begin{array}{c} B \\ 0 \end{array} \right) r $$'];
    text(0, 0, eq1, 'Interpreter', 'latex', 'FontSize', 16);
    
    eq2 = ['$$ y = \left( \begin{array}{cc} C & 0 \end{array} \right) $$', ...
        '$$ \left( \begin{array}{c} x \\ e \end{array} \right) $$'];
    text(3, -2.5, eq2, 'Interpreter', 'latex', 'FontSize', 16);
    
    text(-0.5, -4, ['with controller gain K, state estimator L, ', ...
        'and initial state x(0).'], 'FontSize', 13);
    
    %%
    % Set dimensions and turn axes off.
    % 
    axis([0 10 -6 3]);
    axis off;
end