%% Axes: Draw the phase plot.
%
function phasePlot = drawPhasePlot(w, h, f)
    phasePlot = axes('Units', 'pixels', 'Tag', 'phasePlot', ...
        'Parent', f, ...
        'Position', [23/40*w 1/8*h 2/5*w 2/5*h], ...
        'Units', 'normalized');
    title('Phase Plot');
    axis([-10 10 -10 10]);
    xlabel('x_c(t)');
    ylabel('x_e(t)');
    hold on;
end