%% Axes: Insert the matrix equation for the model.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function problem = drawProblem(w, h, f)

    %%
    % Create the axes.
    % 
    problem = axes('Units', 'pixels', 'Tag', 'problemIntro', 'Parent', f, ...
        'Position', [1/20*w 2/12*h 2/5*w 1/3*h], 'Units', 'normalized');
    
    %%
    % Insert the model equation.
    % 
    message1 = sprintf(['In this module, your goal is to induce and regulate a medically\n'...
        'induced coma in a patient with traumatic brain injury. In particular,\n'...
        'you will titrate the anesthethic drug to maintain a particular burst suppression\n'...
        'ratio in the effect-site of the brain. Denote: \n']);
    
    text(-0.5, 2, message1, 'FontSize', 15);
    
    def1 = ['1. The effect-site drug concentration as $$x_E(t)$$'];
    
    text(-0.5, 0, def1, 'Interpreter', 'latex', 'FontSize', 15);
    
    def2 = ['2. The drug concentration in the central compartment as $$x_C(t)$$'];
    
    text(-0.5, -1.5, def2, 'Interpreter', 'latex', 'FontSize', 15);
    
    def3 = ['3. The burst suppression ratio $$p(t) = \frac{ 1 - exp(-x_E(t)) }{ 1 + exp(-x_E(t))}$$'];

    text(-0.5, -3, def3, 'Interpreter', 'latex', 'FontSize', 15);
    
    def4 = ['4. The rate of infusion of the anesthethic drug that nurses regulate and administer as $$u(t)$$'];
    
    text(-0.5, -4.5, def4, 'Interpreter', 'latex', 'FontSize', 15);    
    
    message = sprintf(['As shown in the figure and in the model, the anesthetic\n'... 
        'drug enters the body and is eliminated from the body through the central\n'... 
        'compartment, and can flow in both directions between the two compartments.\n']);
    
    text(-0.5, -7, message, 'FontSize', 15);

    %%
    % Set dimensions and turn axes off.
    % 
    axis([0 10 -6 3]);
    axis off;
end