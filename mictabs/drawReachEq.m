%% Axes: Insert the details of the reachability matrix.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function reachEq = drawReachEq(w, h, A, B, R, f)

    %%
    % Create the axes.
    % 
    reachEq = axes('Units', 'pixels', 'Tag', 'rEq', 'Parent', f, ...
        'Position', [25/40*w 7/12*h 2/5*w 1/3*h], 'Units', 'normalized');
    
    %%
    % Insert the reachability matrix.
    text(-0.5, 2, ['The reachability matrix can be represented by:'], ...
        'FontSize', 13);
    
    eq1 = '$$ R = \left( \begin{array}{cc} AB &  B \end{array} \right)$$';
    text(0, 0, eq1, 'Interpreter', 'latex', 'FontSize', 16);
    mtx = sprintf(['$$ = \\left( \\begin{array}{cc} %0.3g & %0.3g $$' ...
        '$$ \\\\ %0.3g & %0.3g \\\\ \\end{array} \\right)$$'], ...
        R(1,1), R(1,2), R(2,1), R(2,2));
    text(3.5, 0, mtx, 'Interpreter', 'latex', 'FontSize', 16, ...
        'Tag', 'rMtx');
    determinant = sprintf(['$$ det(R) = %0.3g $$'], det(R));
    text(0, -2.5, determinant, 'Interpreter', 'latex', 'FontSize', 16, ...
        'Tag', 'rDet');
     text(0, -5, 'A =', 'Interpreter', 'latex',  'FontSize', 16);
    Amtx = sprintf(['$$ \\left( \\begin{array}{cc} %0.3g & %0.3g $$' ...
        '$$ \\\\ %0.3g & %0.3g \\\\ \\end{array} \\right)$$'], ...
        A(1,1), A(1,2), A(2,1), A(2,2));
    text(1, -5, Amtx, 'Interpreter', 'latex',  'FontSize', 16, 'Tag', 'reachAMatrix');

    text(0, -8, 'B =', 'Interpreter', 'latex',  'FontSize', 16);
    Bmtx = sprintf(['$$ \\left( \\begin{array}{cc} %0.3g \\\\ %0.3g \\end{array} \\right)$$'], ...
        B(1), B(2));
    text(1, -8, Bmtx, 'Interpreter', 'latex',  'FontSize', 16, 'Tag', 'reachBMatrix');
    
    %%
    % Set dimensions and turn axes off.
    % 
    axis([0 10 -6 3]);
    axis off;
end