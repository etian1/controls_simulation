%% Axes: Plot the response with feedback control.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function responseAxes = drawResponse(w, h, f)

    %%
    % Create the axes.
    % 
    responseAxes = axes('Units', 'pixels', 'Tag', 'rEq', 'Parent', f, ...
        'Position', [10/20*w 1/8*h 7/16*w 1/3*h], 'Units', 'normalized');
    
    %%
    % Set dimensions and parameters.
    % 
    title('System Response');
    axis([0 50 0 50]);
    xlabel('Time');
    ylabel('Closed loop response x_{CL}(t)');
    hold on;
end