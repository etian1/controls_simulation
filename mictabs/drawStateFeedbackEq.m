%% Axes: Insert the state feedback equation.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function stateFeedbackEq = drawStateFeedbackEq(w, h, f)

    %%
    % Create the axes.
    % 
    stateFeedbackEq = axes('Units', 'pixels', 'Tag', 'mtxEq', 'Parent', f, ...
        'Position', [21/40*w 7/12*h 2/5*w 1/3*h], 'Units', 'normalized');
    
    %%
    % Insert the model equation.
    % 
    text(-0.5, 2, ['The state feedback control model (closed loop) ', ...
        'can be represented by:'], 'FontSize', 13);
    eq1 = '$$ \dot{x}_{CL} = (A+BK)x + Br $$';
    text(3, 0, eq1, 'Interpreter', 'latex', 'FontSize', 16);
    eq2 = '$$ y = x_{CL} $$';
    text(3, -2, eq2, 'Interpreter', 'latex', 'FontSize', 16);
    text(-0.5, -3.5, 'with controller gain K and initial state x(0).', ...
        'FontSize', 13);
    
    %%
    % Set dimensions and turn axes off.
    % 
    axis([0 10 -6 3]);
    axis off;
end