%% Helper: Plot an arrow in black with line width 2.
% _dir_ represents the direction of the arrow (r, l, u, or d),
% _(xpos, ypos)_ are the coordinates of the tail, and _s_ is the length.
function plotArrow(dir, xpos, ypos, s)

    %%
    % Horizontal and vertical components of default arrow (right).
    h = [0 s s-0.2 s s-0.2];
    v = [0 0 0.1 0 -0.1];
    
    %%
    % Set the direction of the arrow.
    % 
    if strcmp(dir, 'r')         % right
        x = h;
        y = v;
    elseif strcmp(dir, 'l')     % left
        x = -h;
        y = v;
    elseif strcmp(dir, 'u')     % up
        x = v;
        y = h;
    elseif strcmp(dir, 'd')     % down
        x = v;
        y = -h;
    end
    
    %%
    % Move the arrow to (xpos, ypos) and plot.
    % 
    x = x + xpos;                   % move to xpos
    y = y + ypos;                   % move to ypos
    plot(x, y, 'k', 'LineWidth', 2);
    hold on

end