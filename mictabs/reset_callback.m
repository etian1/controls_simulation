%% Callback: Reset all parameters to default values.
function reset_callback(hObject, callbackdata)
    %%
    % Reset text fields.
    % 
    kce = findobj('Tag', 'kceIn');      % kce = 1
    kce.String = '1';
    kce.Value = 1;
    kec = findobj('Tag', 'kecIn');      % kec = 0.5
    kec.String = '0.5';
    kec.Value = 0.5;
    kco = findobj('Tag', 'kcoIn');      % kco = 1
    kco.String = '1';
    kco.Value = 1;
    u = findobj('Tag', 'uIn');          % u = 1
    u.String = '1';
    u.Value = 1;
    c1 = findobj('Tag', 'c1In');        % c_1 = 1
    c1.String = '1';
    c1.Value = 1;
    c2 = findobj('Tag', 'c2In');        % c_2 = 1
    c2.String = '1';
    c2.Value = 1;
    K1 = findobj('Tag', 'K1In');        % K_1 = 1
    K1.String = '1';
    K1.Value = 1;
    K2 = findobj('Tag', 'K2In');        % K_2 = 1
    K2.String = '1';
    K2.Value = 1;
    L1 = findobj('Tag', 'L1In');        % L_1 = 1
    L1.String = '1';
    L1.Value = 1;
    L2 = findobj('Tag', 'L2In');        % L_2 = 0
    L2.String = '0';
    L2.Value = 0;
    
    
    %%
    % Reset plots and block diagrams.
    % 
    h = guidata(hObject.Parent);
    [a, b, c, d, c1, c2] = updateMtxEq(h.mtxEq);    % matrix equation
    h.A = [a, b; c, d];
    h.C = [c1, c2];
    guidata(hObject.Parent, h);             % save A matrix
    updateReachEq(h.reachEq);               % reachability equation (3)
    updateObsEq(h.obsEq);                   % observability equation (4)
    updateBlockDiag(h.blockDiag, 0, 0);     % block diagram (1)
    cla(h.errorPlot);                       % error plot (4)
    cla(h.stabilityPlot);                   % eigenvalues and poles
    cla(h.concPlot);                        % concentration plot
    axis(h.concPlot, [0 50 0 50]);
    cla(h.phasePlot);                       % phase plot
    cla(h.responsePlot);                    % system response
    cla(h.obsPlot);                         % observer plot
    guidata(hObject.Parent, h);
end

