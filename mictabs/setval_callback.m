%% Callback: Set values of numerical variables.
function setval_callback(hObject, callbackdata)
    %%
    % Convert input to double and check if valid.
    % 
    val = str2double(get(hObject, 'String'));
    if isnan(val)
        errordlg('Please enter a valid input!', 'Invalid Input', 'modal')
    else
        hObject.Value = val;
    end
end