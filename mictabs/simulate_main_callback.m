%% Callback: Run the stability and reachability simulation with the inputted values.
% 
function simulate_main_callback(hObject, callbackdata)
    %%
    % Update the state and the matrix equations.
    %
    h = guidata(hObject.Parent);
    h.run = 1;
    [a, b, c, d, c1, c2] = updateMtxEq(h.mtxEq);    % A matrix
    [L_1, L_2] = updateL();                         % L matrix
    [K_1, K_2] = updateK();                         % K matrix
    h.A = [a, b; c, d];
    h.C = [c1 c2];
    h.L = [L_1; L_2];
    A = h.A;
    B = h.B;
    C = h.C;
    D = h.D;
    L = h.L;
    guidata(hObject.Parent, h);                     % save A matrix
    
    %%
    % Update transfer function
    %
    [ transferFunction ] = updateMtxEqnStability(h.stabilityEq);
    
    
    %%
    % Get the value of the step input _u(t)_.
    % 
    uval = findobj('Tag', 'uIn');
    u = uval.Value;
    
    %%
    % Solve the equations of x_C(t) and x_E(t) using dsolve.
    % Note: requires symbolic math toolbox.
    % 
    syms xc(t) xe(t);
    eq1 = diff(xc) == a * xc + b * xe + h.B(1) * u;
    eq2 = diff(xe) == c * xc + d * xe + h.B(2) * u;
    const1 = xc(0) == h.B(1) * u;
    const2 = xe(0) == h.B(2) * u;
    [xcSol(t), xeSol(t)] = dsolve(eq1, eq2, const1, const2);
    
    %%
    % Solve the equations of e_C(t) and e_E(t), corresponding to the errors, using dsolve.
    % Note: requires symbolic math toolbox.
    % 
    errorMatrix = A + L * C;
    syms e_c(t) e_e(t);
    eq3 = diff(e_c) == errorMatrix(1,1) * e_c + errorMatrix(1,2) * e_e;
    eq4 = diff(e_e) == errorMatrix(2,1) * e_c + errorMatrix(2,2) * e_e;
    const3 = e_c(0) == h.B(1) * u;
    const4 = e_e(0) == h.B(2) * u;
    [e_c_sol(t), e_e_sol(t)] = dsolve(eq3, eq4, const3, const4);
    
    %%
    % Solve the equations of x_CL(t) and y(t) using dsolve.
    syms xcCL(t) xeCL(t);
    temp1 = B * [K_1, K_2];               % B * K
    temp2 = A + temp1;                    % A + BK
    eq5 = diff(xcCL) == temp2(1,1)*xcCL + temp2(1,2)*xeCL;
    eq6 = diff(xeCL) == temp2(2,1)*xcCL + temp2(2,2)*xeCL;
    const5 = xcCL(0) == h.B(1) * u;
    const6 = xeCL(0) == h.B(2) * u;
    [xcCLSol(t), xeCLSol(t)] = dsolve(eq5, eq6, const5, const6);
    
    %%
    % Generate vectors _x_C_ and _x_E_ to plot.
    % 
    STEP = 0.9;
    MAX_x = 50;                                 % x-axis limit
    t_x = 0:STEP:MAX_x;
    xcEq = matlabFunction(xcSol);
    xeEq = matlabFunction(xeSol);
    x_c_y = xcEq(t_x);
    if length(x_c_y) < length(t_x)               % if constant
        x_c_y = x_c_y * ones(1, length(t_x));
    end
    x_e_y = xeEq(t_x);
    if length(x_e_y) < length(t_x)               % if constant
        x_e_y = x_e_y * ones(1, length(t_x));
    end
    MAX_y = max([max(x_c_y), max(x_e_y), 1]);     % y-axis limit
    
    %%
    % Generate vectors e_c(t) and e_e(t) to plot.
    e_e_eq = matlabFunction(e_e_sol);
    e_c_eq = matlabFunction(e_c_sol);
    e_e_yvals = e_e_eq(t_x);
    e_c_yvals = e_c_eq(t_x);
    
    if length(e_e_yvals) < length(t_x)           % if constant
        e_e_yvals = e_e_yvals * ones(1, length(t_x));
    end
    
    if length(e_c_yvals) < length(t_x)           % if constant
        e_c_yvals = e_c_yvals * ones(1, length(t_x));
    end
    MAX_y_error = max([max(e_e_yvals), max(e_c_yvals), 1]);
    
    %%
    % Calculate x_C hat and x_E hat.
    %
    x_c_hat_yvals = x_c_y - e_c_yvals;
    x_e_hat_yvals = x_e_y - e_e_yvals;
    
    %%
    % Generate vectors xcCLEq and xeCLEq to plot.
    xcCLEq = matlabFunction(xcCLSol);
    xeCLEq = matlabFunction(xeCLSol);
    xcCL_y = xcCLEq(t_x);
    if length(xcCL_y) < length(t_x)               % if constant
        xcCL_y = xcCL_y * ones(1, length(t_x));
    end
    xeCL_y = xeCLEq(t_x);
    if length(xeCL_y) < length(t_x)               % if constant
        xeCL_y = xeCL_y * ones(1, length(t_x));
    end
    MAXCL_y = max([max(xcCL_y), max(xeCL_y), 1]);     % y-axis limit
    
    %%
    % Clear previous graphs and reset block diagram colors.
    % 
    cla(h.concPlot);
    axis(h.concPlot, [0 MAX_x 0 max(MAX_y / 4, 50)]);
    cla(h.errorPlot);
    cla(h.responsePlot);
    %axis(h.responsePlot, [0 MAX_x 0 max(MAX_y / 4, 50)]);
    cla(h.obsPlot);
    updateBlockDiag(h.blockDiag, 0, 0);

    %%
    % Update the phase plots.
    % 
    cla(h.phasePlot);
    [x, y] = meshgrid(-10:1:10, -10:1:10);
    dx = a * x + b * y + h.B(1) * u;
    dy = c * x + d * y + h.B(2) * u;
    dyu = dy ./ sqrt(dy.^2 + dx.^2);        % normalize
    dxu = dx ./ sqrt(dy.^2 + dx.^2);
    quiver(h.phasePlot, x, y, dxu, dyu);    % plot
    
    %%
    % Create lines to plot and waitbar to track progress.
    % 
    xcLine = animatedline(h.concPlot, 'Color', 'red');
    xeLine = animatedline(h.concPlot, 'Color', 'blue');
    xcErrorLine = animatedline(h.errorPlot, 'Color', 'red');
    xeErrorLine = animatedline(h.errorPlot, 'Color', 'blue');
    xCHatLine = animatedline(h.obsPlot, 'Color', 'red');
    xEHatLine = animatedline(h.obsPlot, 'Color', 'blue');
    xcCL_Line = animatedline(h.responsePlot, 'Color', 'red');
    xeCL_Line = animatedline(h.responsePlot, 'Color', 'blue');
    xcLine2 = animatedline(h.obsPlot, 'Color', 'green');
    xeLine2 = animatedline(h.obsPlot, 'Color', 'magenta');
    w = waitbar(0, 'Please wait... close to cancel', ...
        'Name', 'Simulation in progress', ...
        'Position', [320 350 360 70], ...
        'WindowStyle', 'modal');
    
    %%
    % Plot x_C and x_E versus time and update block diagram colors.
    % 
    for i = 1:length(t_x)
        if ~ishandle(w)
            break;
        end
        waitbar(i/length(t_x), w);                                  % update waitbar
        addpoints(xcLine, t_x(i), x_c_y(i));                         % x_C
        addpoints(xeLine, t_x(i), x_e_y(i));                         % x_E
        addpoints(xcErrorLine, t_x(i), e_c_yvals(i));                 % x_C_Hat
        addpoints(xeErrorLine, t_x(i), e_e_yvals(i));                 % x_E_Hat
        addpoints(xcCL_Line, t_x(i), xcCL_y(i));                    % x_C_CL
        addpoints(xeCL_Line, t_x(i), xeCL_y(i));                    % x_E_CL
        addpoints(xCHatLine, t_x(i), x_c_hat_yvals(i));                    % x_C_CL
        addpoints(xEHatLine, t_x(i), x_e_hat_yvals(i));                    % x_E_CL
        addpoints(xcLine2, t_x(i), x_c_y(i));                         % x_C
        addpoints(xeLine2, t_x(i), x_e_y(i));                         % x_E

        drawnow;
        updateBlockDiag(h.blockDiag, x_c_y(i)/MAX_y, x_e_y(i)/MAX_y);
    end
    delete(w);                                  % close waitbar
    
    %%
    % Set the legend and update the state.
    % 
    legend(h.concPlot, 'x_C', 'x_E', 'Location', 'northwest');
    legend(h.errorPlot, 'e_C', 'e_E','Location','northwest');
    legend(h.responsePlot, 'x_C^{CL}', 'x_E^{CL}');
    legend(h.obsPlot, {'$\hat{x}_C$', '$\hat{x}_E$', '$x_C$', '$x_E$'}, 'Interpreter', 'latex', 'location', 'northwest');
    h.run = 0;
    guidata(hObject.Parent, h);
    
    %%
    % Update the reachability matrix and eigenvalues.
    % 
    R = updateReachEq(h.reachEq);           % update reachability matrix
    %%
    O = updateObsEq(h.obsEq);    
    
    %%
    % Plot the poles of the transfer function.
    % 
%     A = h.A;            % A matrix
%     B = h.B;            % B matrix
%     C = h.C;            % C matrix
%     D = [0];
    [numerator, denominator] = ss2tf(A, B, C, D);       % state space model
    r = roots(denominator);
    cla(h.stabilityPlot);
    plot(h.stabilityPlot, real(r), imag(r), 'xr');      % plot roots
    hold on;
    Avals = eig(A);
    plot(h.stabilityPlot, real(Avals), imag(Avals), 'ob');  % plot evalues
    legend(h.stabilityPlot, 'roots of H', 'eigenvalues of A');
    
    %{
    %%
    % Plot the zeros of the transfer function.
    % 
    sys = tf(numerator, denominator);                   % transfer function
    [p, ~] = pzmap(sys);                                % pole zero map
    yval = zeros(size(p));
    plot(h.stabilityPlot, p, yval, 'x', 'markersize', 20)
    %}
end