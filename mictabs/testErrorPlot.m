

tspan = [0 50];


e0 = [1; 0];

[t,e] = ode45(@edot, tspan, e0);

figure; plot(t,e(:,1),'-r',t,e(:,2),'-b')
ylim([0 7e15])


% syms xc(t) xe(t);
% eq1 = diff(xc) == a * xc + b * xe;
% eq2 = diff(xe) == c * xc + d * xe;
% const1 = xc(0) == h.B(1) * u;
% const2 = xe(0) == h.B(2) * u;
% [xcSol(t), xeSol(t)] = dsolve(eq1, eq2, const1, const2);


function dedt = edot(t,e)
A = [-1 0.5; 1 0.5];
L = [-1; -1];
C = [1 1];
errorMatrix = A + L * C;

dedt = [errorMatrix(1,1) * e(1) + errorMatrix(1,2) * e(2); ...
    errorMatrix(2,1) * e(1) + errorMatrix(2,2) * e(2)];

end


