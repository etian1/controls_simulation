%% Helper: Update the block diagram with the new values.
% _blockDiag_ are the axes on which the block diagram is drawn.
% centCol and effCol are in [0, 1] and are the desired intensities of
% the central and effect-site compartments, respectively. A larger number
% represents a darker color.
function updateBlockDiag(blockDiag, centCol, effCol)
    %%
    % Central compartment (red).
    % 
    centX = [1 3 3 1 1];
    centY = [-1 -1 1 1 -1];
    fill(blockDiag, centX, centY, [1 1-centCol 1-centCol]);
    
    %%
    % Effect-site compartment (blue).
    % 
    effX = centX + 3;
    effY = centY;
    fill(blockDiag, effX, effY, [1-effCol 1-effCol 1]);
end