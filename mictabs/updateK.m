%% Helper: Update the K matrix with the new values.
% Returns the K matrix [K_1, K_2].
% 
function [K_1, K_2] = updateK()

    %%
    % Find the old values.
    % 
    K_1 = findobj('Tag', 'K1In');
    K_2 = findobj('Tag', 'K2In');
 
    %%
    % Set the new values.
    % 
    K_1 = K_1.Value;
    K_2 = K_2.Value;    

end