%% Helper: Update the L matrix with the new values.
% Returns the L matrix [L_1, L_2].
% 
function [L_1, L_2] = updateL()

    %%
    % Find the old values.
    % 
    L_1 = findobj('Tag', 'L1In');
    L_2 = findobj('Tag', 'L2In');
    
    %%
    % Set the new values.
    % 
    L_1 = L_1.Value;
    L_2 = L_2.Value;    

end