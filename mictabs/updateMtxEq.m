%% Helper: Update the matrix equations with the new values.
% _mtxEq_ are the axes on which the matrix equation is drawn. Returns the
% values of the A matrix (a, b, c, d) and the C matrix (c1, c2).
function [a, b, c, d, c1, c2] = updateMtxEq(mtxEq)
    %%
    % Find the values of kce, kec, kco, c1, and c2.
    % 
    kce = findobj('Tag', 'kceIn');
    kec = findobj('Tag', 'kecIn');
    kco = findobj('Tag', 'kcoIn');
    c_1 = findobj('Tag', 'c1In');
    c_2 = findobj('Tag', 'c2In');
    
    %%
    % Create the A matrix [a b; c d].
    % 
    a = 1 - (kce.Value + kco.Value);
    b = kec.Value;
    c = kce.Value;
    d = 1 - kec.Value;
    Amtx = sprintf(['$$ = \\left( \\begin{array}{cc} %0.3g & %0.3g $$' ...
        '$$ \\\\ %0.3g & %0.3g \\\\ \\end{array} \\right)$$'], a, b, c, d);
    
    %%
    % Create the C matrix [c1, c2].
    c1 = c_1.Value;
    c2 = c_2.Value;
    Cmtx = sprintf(['$$ = log( %0.3g x_{E} + %0.3g x_{C}) $$'], c1, c2);
    
    %%
    % Delete the previous equations and insert the new ones.
    % 
    curr = findobj(mtxEq, 'Tag', 'numMtx');
    delete(curr);
    curr = findobj(mtxEq, 'Tag', 'Cmtx');
    delete(curr);
    text(mtxEq, 1.6, -2.5, Amtx, 'Interpreter', 'latex', 'FontSize', 16, ...
        'Tag', 'numMtx');
    text(mtxEq, 6.87, -5, Cmtx, 'Interpreter', 'latex', 'FontSize', 16,...
        'Tag', 'Cmtx');
end