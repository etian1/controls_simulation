%% Axes: Insert the matrix equation for the model.
% Returns the axes on which the matrix is inserted. _w_ and _h_ represent
% the width and height of the figure window, respectively. f is the parent
% figure.
function [ transferFunction ] = updateMtxEqnStability(stabilityEq)
    %%
    % Get data from parent
    %
    h = guidata(stabilityEq.Parent);
    A = h.A;
    B = h.B;
    C = h.C;
    D = h.D;
    

    %%
    % Insert the model equation.
    % 
%     text(-0.5, 2, ['The transfer function can be represented by:'], 'FontSize', 13);
    
    % variable equation
%     eq1 = ['$$ H(s) = C(sI-A)^{-1}B + D = $$'];
    
%     text(0, 0, eq1, 'Interpreter', 'latex', 'FontSize', 16);
    
    % Display transfer function
    [numerator, denominator] = ss2tf(A, B, C, D);
    sys = tf(numerator, denominator);
    t = evalc('sys');
    t(1:6) = [];
    
    
    %%
    % Delete previous equations and insert new ones
    %
    curr = findobj(stabilityEq, 'Tag', 'transferFunction');
    delete(curr);
    text(stabilityEq, 0, -3, t, 'FontSize', 13, 'Tag', 'transferFunction');
    transferFunction = t;

    
end