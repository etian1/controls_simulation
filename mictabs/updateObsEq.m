%% Helper: Update the observability equation with the new values.
% _obsEq_ are the axes on which the matrix equation is drawn. Returns the
% O matrix.
function O = updateObsEq(obsEq)
    
    %%
    % Get important matrices.
    % 
    h = guidata(obsEq.Parent);
    A = h.A;
    C = h.C;
    O = obsv(A, C);

    %%
    % Delete the previous equation and determinant and insert the new one.
    % 
    mtx = sprintf(['$$ = \\left( \\begin{array}{cc} %0.3g & %0.3g $$' ...
        '$$ \\\\ %0.3g & %0.3g \\\\ \\end{array} \\right)$$'], ...
        O(1,1), O(1,2), O(2,1), O(2,2));
    determinant = sprintf(['$$ det(O) = %0.3g $$'], det(O));
    curr = findobj(obsEq, 'Tag', 'oMtx');
    delete(curr);
    curr = findobj(obsEq, 'Tag', 'oDet');
    delete(curr);
    text(obsEq, 2.5, 0, mtx, 'Interpreter', 'latex', 'FontSize', 16, ...
        'Tag', 'oMtx');
    text(obsEq, 0, -2.5, determinant, 'Interpreter', 'latex', 'FontSize', 16, ...
        'Tag', 'oDet');
    curr = findobj(obsEq, 'Tag', 'obsAMatrix');
    delete(curr);
    curr = findobj(obsEq, 'Tag', 'obsCMatrix');
    delete(curr);
    Amtx = sprintf(['$$ \\left( \\begin{array}{cc} %0.3g & %0.3g $$' ...
        '$$ \\\\ %0.3g & %0.3g \\\\ \\end{array} \\right)$$'], ...
        A(1,1), A(1,2), A(2,1), A(2,2));
    text(obsEq, 1, -5, Amtx, 'Interpreter', 'latex',  'FontSize', 16, 'Tag', 'obsAMatrix');

    Cmtx = sprintf(['$$ \\left( \\begin{array}{cc} %0.3g & %0.3g \\end{array} \\right)$$'], ...
        C(1), C(2));
    text(obsEq, 1, -8, Cmtx, 'Interpreter', 'latex',  'FontSize', 16, 'Tag', 'obsCMatrix');
end