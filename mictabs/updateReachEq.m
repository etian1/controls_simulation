%% Helper: Update the reachability equation with the new values.
% _reachEq_ are the axes on which the matrix equation is drawn. Returns the
% R matrix.
function R = updateReachEq(reachEq)
    
    %%
    % Get important matrices.
    % 
    h = guidata(reachEq.Parent);
    A = h.A;
    B = h.B;
    R = fliplr(ctrb(A, B));

    %%
    % Delete the previous equation and determinant and insert the new one.
    % 
    mtx = sprintf(['$$ = \\left( \\begin{array}{cc} %0.3g & %0.3g $$' ...
        '$$ \\\\ %0.3g & %0.3g \\\\ \\end{array} \\right)$$'], ...
        R(1,1), R(1,2), R(2,1), R(2,2));
    determinant = sprintf(['$$ det(R) = %0.3g $$'], det(R));
    curr = findobj(reachEq, 'Tag', 'rMtx');
    delete(curr);
    curr = findobj(reachEq, 'Tag', 'rDet');
    delete(curr);
    text(reachEq, 3.5, 0, mtx, 'Interpreter', 'latex', 'FontSize', 16, ...
        'Tag', 'rMtx');
    text(reachEq, 0, -2.5, determinant, 'Interpreter', 'latex', 'FontSize', 16, ...
        'Tag', 'rDet');
    
    curr = findobj(reachEq, 'Tag', 'reachAMatrix');
    delete(curr);
    curr = findobj(reachEq, 'Tag', 'reachBMatrix');
    delete(curr);
    
    Amtx = sprintf(['$$ \\left( \\begin{array}{cc} %0.3g & %0.3g $$' ...
        '$$ \\\\ %0.3g & %0.3g \\\\ \\end{array} \\right)$$'], ...
        A(1,1), A(1,2), A(2,1), A(2,2));
    text(reachEq, 1, -5, Amtx, 'Interpreter', 'latex',  'FontSize', 16, 'Tag', 'reachAMatrix');

    Bmtx = sprintf(['$$ \\left( \\begin{array}{cc} %0.3g \\\\ %0.3g \\end{array} \\right)$$'], ...
        B(1), B(2));
    text(reachEq, 1, -8, Bmtx, 'Interpreter', 'latex',  'FontSize', 16, 'Tag', 'reachBMatrix');
end